import { HomeOutlined, ShoppingCartOutlined } from "@ant-design/icons";
import CategoryPage from "../page/CategoryPage/CategoryPage";
import DetailPage from "../page/DetailPage/DetailPage";
import Cart from "../page/HomePage/Cart/Cart";
import HomePage from "../page/HomePage/HomePage";
import LoginPage from "../page/LoginPage/LoginPage";
import Page404 from "../page/Page404/Page404";
import RegisterPage from "../page/RegisterPage/RegisterPage";

export const userRouter = [
  {
    path: "/",
    component: <HomePage />,
    exact: true,
    icon: <HomeOutlined />,
    title: "Trang chủ",
    isUseLayout: true,
  },
  {
    path: "/login",
    component: <LoginPage />,
    exact: true,
    icon: <HomeOutlined />,
    title: "Trang đăng nhập",
    isUseLayout: true,
  },
  {
    path: "/register",
    component: <RegisterPage />,
    exact: true,
    icon: <HomeOutlined />,
    title: "Trang đăng ký",
    isUseLayout: true,
  },
  {
    path: "/course/:id",
    component: <DetailPage />,
    exact: true,
    icon: <HomeOutlined />,
    title: "Trang chi tiết khoá học",
    isUseLayout: true,
  },
  {
    path:"/courses/:id",
    component: <CategoryPage/>,
    icon: <HomeOutlined />,
    title: "Trang danh mục khoá học",
    isUseLayout: true,
  },
  {
    path:"/cart",
    component: <Cart/>,
    exact: true,
    icon: <ShoppingCartOutlined/>,
    title: "Trang thanh toán khoá học",
    isUseLayout: true,
  }
];
