const KEY = "key";

export const localStorageService = {
  removeUserInfo: () => {
    localStorage.removeItem(KEY);
  },
  setUserInfo: (data) => {
    localStorage.setItem(KEY, JSON.stringify(data));
  },
  getUserInfo: () => {
    let dataJson = localStorage.getItem(KEY);
    if (dataJson) {
      return JSON.parse(dataJson);
    }
    return null;
  },
};
