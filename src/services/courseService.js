import { httpService } from "./configURL";

export const courseService = {
  getDanhSachKhoaHoc: () => {
    return httpService.get("/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01");
  },
  getChiTietKhoaHoc: (maKhoaHoc) => {
    return httpService.get(
      `/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${maKhoaHoc}`
    );
  },
  getDSKhoaHocTheoDanhMuc: (maDanhMuc) => {
    return httpService.get(
      `/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${maDanhMuc}&MaNhom=GP01`
    );
  },
  getDanhMucKhoaHoc: () => {
    return httpService.get(`/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc`);
  },
  postDangKyKhoaHoc: (data) => {
    return httpService.post(`/api/QuanLyKhoaHoc/DangKyKhoaHoc`, data);
  },
  getThongTinHocVienKhoaHoc: (maKhoaHoc) => {
    return httpService.get(
      `/api/QuanLyKhoaHoc/LayThongTinHocVienKhoaHoc?maKhoaHoc=${maKhoaHoc}`
    );
  },
};
