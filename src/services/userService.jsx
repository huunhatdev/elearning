import { httpService } from "./configURL";

export const userService = {
  postLogin: (dataLogin) => {
    return httpService.post("/api/QuanLyNguoiDung/DangNhap", dataLogin);
  },
  postRegister: (dataSignUp) => {
    return httpService.post("/api/QuanLyNguoiDung/DangKy", dataSignUp);
  },
};
