import axios from "axios";
import { handleLoading } from "../redux/Slices/userSlice";
import { store } from "../redux/store";
import { localStorageService } from "./localStorageService";

const BASE_URL = "https://elearningnew.cybersoft.edu.vn";

export const httpService = axios.create({
  baseURL: BASE_URL,
  headers: {
    TokenCybersoft:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwMyIsIkhldEhhblN0cmluZyI6IjEyLzEyLzIwMjIiLCJIZXRIYW5UaW1lIjoiMTY3MDgwMzIwMDAwMCIsIm5iZiI6MTY0NzUzNjQwMCwiZXhwIjoxNjcwOTUwODAwfQ.p9HfB3wfHiMPOhk-B2tIt1JOp-IxfMXGRoFv610OjtY",
  },
});
httpService.interceptors.request.use(
  function (config) {
    store.dispatch(handleLoading(true));
    httpService.defaults.headers.common["Authorization"] =
      "Bearer " + localStorageService.getUserInfo()?.accessToken;
    return config;
  },
  function (err) {
    store.dispatch(handleLoading(false));
    return Promise.reject(err);
  }
);
httpService.interceptors.response.use(
  function (config) {
    store.dispatch(handleLoading(false));
    return config;
  },
  function (err) {
    store.dispatch(handleLoading(false));
    return Promise.reject(err);
  }
);
