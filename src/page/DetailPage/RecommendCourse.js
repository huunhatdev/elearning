import React, { useEffect, useState } from "react";
import {
  StarFilled,
  UsergroupAddOutlined,
  HeartOutlined,
} from "@ant-design/icons";
import { courseService } from "../../services/courseService";
import { NavLink } from "react-router-dom";
export default function RecommendCourse({ course }) {
  let [btnShowMore, setBtnShowMore] = useState(false);
  let [courseList, setCourseList] = useState([]);
  useEffect(() => {
    let fetchData = async () => {
      let result = await courseService.getDSKhoaHocTheoDanhMuc(
        course.danhMucKhoaHoc?.maDanhMucKhoahoc
      );
      setCourseList(result.data);
    };
    fetchData();
  }, [course]);
  return (
    <div>
      <h1 className="text-3xl font-bold mt-5 text-left">
        Students also bought
      </h1>
      <div
        className={
          btnShowMore ? "border-b mb-5" : "border-b mb-5 overflow-hidden"
        }
        style={btnShowMore ? { height: "max-content" } : { height: "448px" }}
        id="recommend-course"
      >
        {courseList?.map((course) => {
          return (
            <NavLink
              className="grid grid-cols-12 justify-center items-center border-b py-4 h-28 text-black hover:text-black"
              to={`/course/${course.maKhoaHoc}`}
            >
              <div className="col-span-2 flex justify-start items-center">
                <img
                  style={{ width: "80px", height: "80px" }}
                  src={course.hinhAnh}
                  alt="course"
                ></img>
              </div>
              <div className="text-left col-span-5">
                <h1 className="font-bold">{course.tenKhoaHoc}</h1>
                <span>Updated {course.ngayTao}</span>
              </div>
              <div
                className="flex justify-center items-center gap-1"
                style={{ color: "#e59819" }}
              >
                <span>4.5</span>
                <StarFilled />
              </div>
              <div className="col-span-2 flex justify-center items-center gap-1">
                <UsergroupAddOutlined />
                <span>{course.soLuongHocVien}</span>
              </div>
              <div className="flex flex-col justify-start items-center">
                <span className="font-bold text-sm">$16.99</span>{" "}
                <span
                  className="line-through text-xs"
                  style={{ color: "#6a6f73" }}
                >
                  $84.99
                </span>
              </div>
              <div className="flex justify-end items-end">
                <button className="border border-black hover:bg-slate-100 rounded-full w-10 h-10 flex justify-center items-center">
                  <HeartOutlined style={{ fontSize: "16px" }} />
                </button>
              </div>
            </NavLink>
          );
        })}
      </div>
      {courseList.length > 4 && (
        <div id="show-course-btn" className="mb-5">
          <button
            id="show-more-btn"
            className={
              btnShowMore
                ? "border-black border w-full py-2 hidden"
                : " border-black border w-full py-2"
            }
            onClick={() => {
              setBtnShowMore(true);
            }}
          >
            <span className="font-bold text-sm">Show more</span>
          </button>
          <button
            className={
              !btnShowMore
                ? "hidden border-black border w-full py-2"
                : "border-black border w-full py-2"
            }
            id="show-less-btn"
            onClick={() => {
              setBtnShowMore(false);
            }}
          >
            <span className="font-bold text-sm">Show less</span>
          </button>
        </div>
      )}
    </div>
  );
}

// {
//     "maKhoaHoc": "0909888777",
//     "biDanh": "hoc-ve-lap-trinh-web",
//     "tenKhoaHoc": "Hoc ve lap trinh web",
//     "moTa": "123",
//     "luotXem": 100,
//     "hinhAnh": "https://elearning0706.cybersoft.edu.vn/hinhanh/hoc-ve-lap-trinh-web_gp01.png",
//     "maNhom": "GP01",
//     "ngayTao": "21/07/2022",
//     "soLuongHocVien": 0,
//     "nguoiTao": {
//         "taiKhoan": "khanhpham",
//         "hoTen": "Pham Viet Khanh",
//         "maLoaiNguoiDung": "GV",
//         "tenLoaiNguoiDung": "Giáo vụ"
//     },
//     "danhMucKhoaHoc": {
//         "maDanhMucKhoahoc": "FrontEnd",
//         "tenDanhMucKhoaHoc": "Lập trình Front end"
//     }
// }
