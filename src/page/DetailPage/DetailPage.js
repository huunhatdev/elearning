import React, { useEffect, useState } from "react";
import { Breadcrumb } from "antd";
import { courseService } from "../../services/courseService";
import { useNavigate, useParams } from "react-router";
import { Rate, Collapse, Tag } from "antd";
import StickyBox from "react-sticky-box";
import swal from "sweetalert";
import {
  CheckOutlined,
  CaretUpOutlined,
  CaretDownOutlined,
  ExclamationCircleOutlined,
  GlobalOutlined,
  ClockCircleOutlined,
  YoutubeOutlined,
  RedoOutlined,
  MobileOutlined,
  TrophyOutlined,
} from "@ant-design/icons";
import "./DetailPage.css";
import RecommendCourse from "./RecommendCourse";
import { useDispatch } from "react-redux";
import { addItem } from "../../redux/Slices/cartSlice";
import { localStorageService } from "../../services/localStorageService";
export default function DetailPage() {
  const maKhoaHoc = useParams();
  const [detailCourse, setDetailCourse] = useState({});
  const { Panel } = Collapse;
  let [isDetailExpand, setDetailExpand] = useState(false);
  let [descriptionExpand, setDescriptionExpand] = useState(false);
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const [listHV, setListHV] = useState([]);
  useEffect(() => {
    let fetchData = async () => {
      let result = await courseService.getChiTietKhoaHoc(maKhoaHoc.id);
      setDetailCourse(result.data);
    };
    fetchData();
    document.getElementById("searchList").style.display = "none";
  }, [maKhoaHoc.id]);

  useEffect(() => {
    let fetchData = async () => {
      let result = await courseService.getThongTinHocVienKhoaHoc(maKhoaHoc.id);
      setListHV(result.data.lstHocVien);
    };
    fetchData();
  }, [maKhoaHoc.id]);
  useEffect(() => {
    let boxContent = document.getElementById("box-content");
    let descriptionBox = document.getElementById("description-box");

    if (boxContent.clientHeight < 150) {
      document.getElementById("expand-btn").className = "hidden";
      boxContent.className = "show-less--content";
    } else {
      document.getElementById("expand-btn").className =
        "flex justify-start items-start";
    }
    if (descriptionBox.clientHeight < 150) {
      document.getElementById("expand-btn-moTa").className = "hidden";
      descriptionBox.className = "show-less--content";
    } else {
      document.getElementById("expand-btn-moTa").className =
        "flex justify-start items-start";
      descriptionBox.className = "show-more--content";
    }
  }, [detailCourse]);
  return (
    <div className="relative h-full">
      <div>
        {/* Course Name */}
        <div id="course-name" className="bg-black mb-5">
          <div className="container mx-auto w-2/3">
            <div className="py-5">
              <Breadcrumb separator=">">
                <Breadcrumb.Item href="">
                  {detailCourse.danhMucKhoaHoc?.tenDanhMucKhoaHoc}
                </Breadcrumb.Item>
                <Breadcrumb.Item href="">
                  {detailCourse.maKhoaHoc}
                </Breadcrumb.Item>
              </Breadcrumb>
              
              <div className="text-white text-left my-5 w-2/3">
                <h1 className=" text-white text-3xl font-bold">
                  {detailCourse.tenKhoaHoc}
                </h1>
                <div>
                  <Tag color="cyan">Best seller</Tag>
                  <span className="font-bold" style={{ color: "#f3ca8c" }}>
                    4.5
                  </span>
                  <Rate
                    disabled
                    allowHalf
                    defaultValue={4.5}
                    style={{ fontSize: 14, color: "#f3ca8c", margin: "0 4px" }}
                  />
                  <span className="mx-1" style={{ color: "#b4a8dc" }}>
                    (
                    <a
                      href=""
                      className="underline hover:underline"
                      style={{ color: "#b4a8dc" }}
                    >
                      5 ratings
                    </a>
                    )
                  </span>
                  <span>{detailCourse.soLuongHocVien} students</span>
                </div>
                <div className="my-2">
                  <span>Created by</span>
                  <span className="underline mx-2" style={{ color: "#b4a8dc" }}>
                    {detailCourse.nguoiTao?.hoTen}
                  </span>
                </div>
                <div className="flex justify-start items-center gap-3">
                  <div className="flex justify-start items-center gap-2">
                    <ExclamationCircleOutlined />
                    <p className="mb-0">Last updated {detailCourse.ngayTao}</p>
                  </div>
                  <div className="flex justify-start items-center gap-2">
                    <GlobalOutlined />
                    <p className="mb-0">English</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container mx-auto w-full xl:w-2/3">
          <div className=" w-full xl:w-2/3">
            {/* Course detail */}
            <div className="flex flex-col" style={{ border: "1px solid gray" }}>
              <div
                id="course-detail"
                className="container p-5 justify-start items-start"
              >
                <h2 className="text-left text-2xl font-bold">
                  What you'll learn
                </h2>
                <div
                  id="box-content"
                  className={
                    isDetailExpand ? "show-less--content" : "show-more--content"
                  }
                >
                  <div
                    className="grid grid-cols-2 gap-2"
                    style={{ color: "#555657" }}
                  >
                    <div className="flex justify-start items-start text-left">
                      <div>
                        <CheckOutlined />
                      </div>
                      <span className="mx-4">
                        Lorem, ipsum dolor sit amet consectetur adipisicing
                        elit. Recusandae totam dolorum reprehenderit facilis
                        doloribus!
                      </span>
                    </div>
                    <div className="flex justify-start items-start text-left">
                      <div>
                        <CheckOutlined />
                      </div>
                      <span className="mx-4">
                        Lorem, ipsum dolor sit amet consectetur adipisicing
                        elit. Recusandae totam dolorum reprehenderit facilis
                        doloribus!
                      </span>
                    </div>
                    <div className="flex justify-start items-start text-left">
                      <div>
                        <CheckOutlined />
                      </div>
                      <span className="mx-4">
                        Lorem, ipsum dolor sit amet consectetur adipisicing
                        elit. Recusandae totam dolorum reprehenderit facilis
                        doloribus!
                      </span>
                    </div>
                    <div className="flex justify-start items-start text-left">
                      <div>
                        <CheckOutlined />
                      </div>
                      <span className="mx-4">
                        Lorem, ipsum dolor sit amet consectetur adipisicing
                        elit. Recusandae totam dolorum reprehenderit facilis
                        doloribus!
                      </span>
                    </div>
                    <div className="flex justify-start items-start text-left">
                      <div>
                        <CheckOutlined />
                      </div>
                      <span className="mx-4">
                        Lorem, ipsum dolor sit amet consectetur adipisicing
                        elit. Recusandae totam dolorum reprehenderit facilis
                        doloribus!
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div id="expand-btn" className="flex justify-start items-start">
                <button
                  className={isDetailExpand ? "hidden" : "pl-5 pb-5"}
                  style={{ color: "#653ad4" }}
                  onClick={() => {
                    setDetailExpand(true);
                  }}
                >
                  <div className="flex justify-start items-center font-bold">
                    <span>Show more</span>
                    <CaretDownOutlined className="mx-2" />
                  </div>
                </button>
                <button
                  className={isDetailExpand ? "pl-5 pb-5" : "hidden"}
                  style={{ color: "#653ad4" }}
                  onClick={() => {
                    setDetailExpand(false);
                  }}
                >
                  <div className=" flex justify-start items-center font-bold">
                    <span>Show less</span>
                    <CaretUpOutlined className="mx-2" />
                  </div>
                </button>
              </div>
            </div>

            {/* Course content */}
            <div id="course-content" className="text-left">
              <h1 className="text-3xl font-bold mt-5">Course content</h1>
              <p>15 sections - 146 lectures - 14h 42m total length </p>
              <div>
                <Collapse>
                  <Panel
                    header={
                      <div className="flex justify-between items-end">
                        <span className="font-bold text-xl">The Basic</span>
                        <span>11 lectures - 53min</span>
                      </div>
                    }
                    key="1"
                  >
                    <ul className="list-disc mx-5 mb-0 leading-7">
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                    </ul>
                  </Panel>
                  <Panel
                    header={
                      <div className="flex justify-between items-end">
                        <span className="font-bold text-xl">
                          Conditions and Loops
                        </span>
                        <span>13 lectures - 57min</span>
                      </div>
                    }
                    key="2"
                  >
                    <ul className="list-disc mx-5 mb-0 leading-7">
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                    </ul>
                  </Panel>
                  <Panel
                    header={
                      <div className="flex justify-between items-end">
                        <span className="font-bold text-xl">Function</span>
                        <span>7 lectures - 26min</span>
                      </div>
                    }
                    key="3"
                  >
                    <ul className="list-disc mx-5 mb-0 leading-7">
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                      <li>Lorem ipsum dolor sit amet.</li>
                    </ul>
                  </Panel>
                </Collapse>
              </div>
            </div>

            {/* Course requirement */}
            <div id="course-requirement" className="text-left">
              <h1 className="text-3xl font-bold mt-5">Requirements</h1>
              <ul className="list-disc mb-0 leading-7 list-inside">
                <li>Macintosh (OSX)/ Windows(Vista and higher) Machine</li>
                <li>Internet Connection</li>
              </ul>
            </div>

            {/* Course description */}
            <div id="course-description" className="text-left">
              <h1 className="text-3xl font-bold mt-5">Description</h1>
              <div
                id="description-box"
                className={
                  descriptionExpand
                    ? "show-less--content"
                    : "show-more--content"
                }
              >
                <p className="m-0 text-base">{detailCourse.moTa}</p>
              </div>
              <div
                id="expand-btn-moTa"
                className="flex justify-start items-start"
              >
                <button
                  className={descriptionExpand ? "hidden" : "py-5"}
                  style={{ color: "#653ad4" }}
                  onClick={() => {
                    setDescriptionExpand(true);
                  }}
                >
                  <div className="flex justify-start items-center font-bold">
                    <span>Show more</span>
                    <CaretDownOutlined className="mx-2" />
                  </div>
                </button>
                <button
                  className={descriptionExpand ? "py-5" : "hidden"}
                  style={{ color: "#653ad4" }}
                  onClick={() => {
                    setDescriptionExpand(false);
                  }}
                >
                  <div className=" flex justify-start items-center font-bold">
                    <span>Show less</span>
                    <CaretUpOutlined className="mx-2" />
                  </div>
                </button>
              </div>
            </div>
            <RecommendCourse course={detailCourse} />
          </div>
        </div>
      </div>
      <div className="absolute top-10 right-40 h-full w-80">
        {/* Course side panel */}
        <StickyBox className="shadow-lg">
          <div className="py-5">
            <div className="relative" id="course-trailer">
              <img
                src={detailCourse.hinhAnh}
                alt="course logo"
                style={{ width: "100%", height: "200px" }}
              />
              <div id="image-cover">
                <p className="absolute bottom-1 w-full font-bold text-white opacity-0">
                  Preview this course
                </p>
                <button className="hidden" id="trailer-btn">
                  <img src="/playbutton.png" alt="play button"></img>
                </button>
              </div>
            </div>
            <div className="container px-4 mb-5">
              <p className="flex justify-start items-center mt-4 mb-0">
                <span className="font-bold text-2xl">$16.99</span>{" "}
                <span
                  className="line-through text-base mx-2"
                  style={{ color: "#6a6f73" }}
                >
                  $84.99
                </span>
                <span className="text-base">80% off</span>
              </p>
              <p className="flex justify-start items-center mb-4 text-red-700">
                <ClockCircleOutlined />
                <span className="mx-1 font-bold">2 days</span>
                <span>left at this price!</span>
              </p>
              <button
                className=" w-full text-white font-bold py-4 text-base mb-2 bg-purple-600 hover:bg-purple-700"
                onClick={() => {
                  let index = listHV.findIndex((item) => {
                    return (
                      item.taiKhoan ===
                      localStorageService.getUserInfo()?.taiKhoan
                    );
                  });
                  if (index !== -1) {
                    swal({
                      title: "Bạn đã đăng ký khoá học này",
                      text: "Vui lòng kiểm tra danh sách khóa học đã đăng ký",
                      icon: "error",
                      button: "Đồng ý",
                    });
                  } else {
                    dispatch(addItem(detailCourse));
                  }
                }}
              >
                Add to cart
              </button>
              <button
                className="w-full font-bold py-4 border-black border text-base hover:bg-slate-100"
                onClick={() => {
                  let index = listHV.findIndex((item) => {
                    return (
                      item.taiKhoan ===
                      localStorageService.getUserInfo()?.taiKhoan
                    );
                  });
                  if (index !== -1) {
                    swal({
                      title: "Bạn đã đăng ký khoá học này",
                      text: "Vui lòng kiểm tra danh sách khóa học đã đăng ký",
                      icon: "error",
                      button: "Đồng ý",
                    });
                  } else {
                    dispatch(addItem(detailCourse));
                    navigate("/cart");
                  }
                }}
              >
                Buy now
              </button>
              <p className="my-2 text-xs font-extralight">
                30-day Money-Back Guarantee
              </p>
              <p className="font-bold text-base text-left mb-2">
                This course includes:
              </p>
              <ul className=" grid grid-cols-1 gap-2 text-left">
                <li className="flex justify-start items-center gap-2">
                  <YoutubeOutlined />
                  14 hours on-demand video
                </li>
                <li className="flex justify-start items-center gap-2">
                  <RedoOutlined />
                  Full lifetime access
                </li>
                <li className="flex justify-start items-center gap-2">
                  <MobileOutlined />
                  Access on mobile and TV
                </li>
                <li className="flex justify-start items-center gap-2">
                  <TrophyOutlined />
                  Certificate of completion
                </li>
              </ul>
              <div className="flex justify-between font-bold underline underline-offset-2">
                <span>Share</span>
                <span>Gift this course</span>
                <span>Apply Coupon</span>
              </div>
            </div>
          </div>
        </StickyBox>
      </div>
    </div>
  );
}

// {
//   "maKhoaHoc": "REACT2021",
//   "biDanh": "khoa-hoc-reactjs-2021",
//   "tenKhoaHoc": "Khóa học reactjs 2021",
//   "moTa": "Become a Senior React Developer! Build a massive E-commerce app with Redux, Hooks, GraphQL, ContextAPI, Stripe, Firebase nhando",
//   "luotXem": 100,
//   "hinhAnh": "https://elearning0706.cybersoft.edu.vn/hinhanh/khoa-hoc-reactjs-2021.jpg",
//   "maNhom": "GP01",
//   "ngayTao": "10/09/2021",
//   "soLuongHocVien": 0,
//   "nguoiTao": {
//     "taiKhoan": "nguyenvana",
//     "hoTen": "Nguyen Van A",
//     "maLoaiNguoiDung": "GV",
//     "tenLoaiNguoiDung": "Giáo vụ"
//   },
//   "danhMucKhoaHoc": {
//     "maDanhMucKhoahoc": "DiDong",
//     "tenDanhMucKhoaHoc": "Lập trình di động"
//   }
// }
