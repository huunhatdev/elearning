import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { handleDeleteCourse } from "../../redux/Slices/userSlice";
export default function CartPage() {
  let dispatch = useDispatch();
  let cart = useSelector((state) => {
    return state.userSlice.cart;
  });
  return (
    <div className=" container mx-auto px-48 py-12">
      <h2 className=" text-left text-4xl font-bold mb-12">Shopping Cart</h2>
      <div className=" flex">
        <div className=" w-3/4">
          <p className=" text-left text-lg font-medium">
            {cart.length} Courses in Cart
          </p>
          {cart.map((course) => {
            return (
              <div className=" flex gap-7">
                <div className=" flex gap-5 w-full">
                  <img src={course.hinhAnh} className=" w-1/6 h-22" alt="" />
                  <div className=" text-left w-2/6">
                    <h6 className="font-bold">{course.tenKhoaHoc}</h6>
                    <h6>Người tạo khóa học : {course.nguoiTao.hoTen}</h6>
                    <h6>Lượt xem : {course.luotXem}</h6>
                    <h6>{course.danhMucKhoaHoc.tenDanhMucKhoaHoc}</h6>
                  </div>
                  <h3
                    onClick={() => {
                      dispatch(handleDeleteCourse(course));
                    }}
                    className=" w-1/6 hover:underline cursor-pointer"
                    style={{ color: "#a435f0" }}
                  >
                    remove
                  </h3>
                  <div
                    className=" w-1/6 font-bold"
                    style={{ color: "#a435f0" }}
                  >
                    $ {course.price}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        <div className=" text-left w-1/4">
          <h4 className=" text-xl font-medium">Total :</h4>
          <h1 className=" text-4xl font-bold">
            $
            {cart.reduce((total, item) => {
              return total + item.price;
            }, 0)}
          </h1>
          <div
            className=" w-full text-center font-bold text-white text-xl py-3 cursor-pointer"
            style={{ backgroundColor: "#a435f0" }}
          >
            BUY
          </div>
        </div>
      </div>
    </div>
  );
}
// {
//     "maKhoaHoc": "TTT777",
//     "biDanh": "javascript",
//     "tenKhoaHoc": "JavaScript",
//     "moTa": "chua mo ta",
//     "luotXem": 100,
//     "hinhAnh": "https://elearning0706.cybersoft.edu.vn/hinhanh/javascript_gp01.jpg",
//     "maNhom": "GP01",
//     "ngayTao": "10/10/2020",
//     "soLuongHocVien": 0,
//     "nguoiTao": {
//         "taiKhoan": "LongDangDn05",
//         "hoTen": "Dang Tien Long",
//         "maLoaiNguoiDung": "GV",
//         "tenLoaiNguoiDung": "Giáo vụ"
//     },
//     "danhMucKhoaHoc": {
//         "maDanhMucKhoahoc": "FrontEnd",
//         "tenDanhMucKhoaHoc": "Lập trình Front end"
//     },
//     "price": 12.99
// }
