import React from "react";
import { Button, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import "../css/page.css";
import { userService } from "../../services/userService";
import { localStorageService } from "../../services/localStorageService";
import { setUserInfor } from "../../redux/Slices/userSlice";

export default function LoginPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    userService
      .postLogin(values)
      .then((res) => {
        dispatch(setUserInfor(res.data));
        localStorageService.setUserInfo(res.data);
        message.success("Đăng nhập thành công");
        setTimeout(() => {
         navigate("/");
        }, 2000);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {};
  return (
    <div className="container mx-auto ">
      <div className=" w-96 mx-auto pt-12">
        <h3 className=" text-base font-bold text-left">
          Log In to Your Udemy Account!
        </h3>
        <div style={{ borderTop: "1px solid #d1d7dc" }} className="pt-3">
          <Form
            name="basic"
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            initialValues={{
              remember: true,
            }}
            autoComplete="off"
          >
            <Form.Item
              label={<p className="text-white">Tài khoản</p>}
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input placeholder="✉️ Tài khoản" />
            </Form.Item>
            <Form.Item
              label={<p className="text-white">Mật khẩu</p>}
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password placeholder="🔒 Mật khẩu" />
            </Form.Item>
            <div className=" pb-5 flex">
              <Button htmlType="submit">
                <h3 className=" text-white font-bold text-center">Log In</h3>
              </Button>
            </div>
            <span className=" text-black my-3">
              Or{"  "}
              <span className=" cursor-pointer" style={{ color: "#5624d0" }}>
                Forgot Password
              </span>
            </span>
            <h4 className=" text-base mb-16">
              Don't have an account ?{" "}
              <NavLink to={"/register"}>
                <span
                  className=" cursor-pointer underline font-bold"
                  style={{ color: "#5624d0" }}
                >
                  {" "}
                  Sign Up
                </span>
              </NavLink>
            </h4>
          </Form>
        </div>
      </div>
    </div>
  );
}
