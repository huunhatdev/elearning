import React from "react";
import { Form, Input, message, Select } from "antd";
import { useNavigate } from "react-router-dom";
import "../css/page.css";
import { userService } from "../../services/userService";
export default function RegisterPage() {
  let navigate = useNavigate();
  const onFinish = (values) => {
    userService
      .postRegister(values)
      .then((res) => {
        message.success("Đăng ký thành công");
        navigate("/login");
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };
  const onFinishFailed = (errorInfo) => {};
  return (
    <div className="container mx-auto ">
      <div className=" w-96 mx-auto py-12">
        <h3 className=" text-base font-bold text-left">
          Sign up and start learning
        </h3>
        <div style={{ borderTop: "1px solid #d1d7dc" }} className="pt-3">
          <Form
            name="basic"
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            initialValues={{
              remember: true,
            }}
            autoComplete="off"
          >
            <Form.Item
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập họ tên",
                  pattern: new RegExp(
                    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/
                  ),
                },
              ]}
            >
              <Input placeholder="Họ Tên" />
            </Form.Item>
            <Form.Item
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tài khoản",
                  pattern: new RegExp(
                    /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/
                  ),
                },
              ]}
            >
              <Input placeholder="Tài khoản" />
            </Form.Item>
            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu",
                  pattern: new RegExp(
                    /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/
                  ),
                },
              ]}
            >
              <Input.Password placeholder="Mật khẩu" />
            </Form.Item>
            <Form.Item
              label={<p className="text-white">Email</p>}
              name="email"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập đúng email",
                  pattern: new RegExp(
                    /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/
                  ),
                },
              ]}
            >
              <Input placeholder="Email" />
            </Form.Item>
            <Form.Item
              label={<p className="text-white">Số điện thoại</p>}
              name="soDT"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập đúng số điện thoại",
                  pattern: new RegExp(/^[0-9]*$/),
                },
              ]}
            >
              <Input placeholder="Số điện thoại" />
            </Form.Item>
            <Form.Item
              name="maNhom"
              rules={[
                {
                  required: true,
                  message: "Vui lòng chọn mã nhóm",
                },
              ]}
            >
              <Select placeholder="Chọn mã nhóm">
                <Select.Option value="GP01">GP01</Select.Option>
                <Select.Option value="GP02">GP02</Select.Option>
                <Select.Option value="GP03">GP03</Select.Option>
              </Select>
            </Form.Item>
            <div className=" w-96 h-12" style={{ backgroundColor: "#a435f0" }}>
              <button
                type="submit"
                className="text-white font-bold w-full h-full"
              >
                SIGN UP
              </button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}
