import { Tag, Rate } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { removeItem } from "../../../redux/Slices/cartSlice";
import { TagFilled } from "@ant-design/icons";

export default function CartItem({ data, sale, price }) {
  let dispatch = useDispatch();
  return (
    <div className="flex justify-between items-start border p-2">
      <img
        src={data.hinhAnh}
        alt=""
        style={{ width: "150px", height: "100px" }}
      ></img>
      <div className="grow mx-2">
        <p className="font-bold text-base mb-0">{data.tenKhoaHoc}</p>
        <p className=" text-xs text-gray-500 font-light">
          {data.nguoiTao.hoTen}
        </p>
        <div className="flex">
          <Tag color="geekblue">Updated Recently</Tag>
          <div className="font-bold text-sm" style={{ color: "#b4690f" }}>
            4.5
          </div>
          <Rate
            disabled
            allowHalf
            defaultValue={4.5}
            style={{
              fontSize: 12,
              color: "#e59819",
              margin: "0 4px",
            }}
          />
          <div className="text-xs text-gray-500 font-light">
            ({data.luotXem} ratings)
          </div>
        </div>
        <p className=" text-xs text-gray-500 font-light">
          2.5 total hours • 42 lectures • All Levels
        </p>
      </div>
      <div className=" mx-20">
        <button
          className="text-violet-700"
          onClick={() => {
            dispatch(removeItem(data));
          }}
        >
          Remove
        </button>
      </div>
      <div className="flex justify-start items-start h-full">
        <div className="flex flex-col">
          <span className="font-bold text-base text-violet-700">
            ${(price * (1 - sale)).toFixed(2)}
          </span>{" "}
          <span
            className="line-through text-sm mx-2 font-light"
            style={{ color: "#6a6f73" }}
          >
            ${price}
          </span>
        </div>
        <TagFilled
          style={{
            lineHeight: "1.5rem",
            fontSize: "16px",
            color: "#7637db",
            transform: "rotateY(3.142rad)",
          }}
        />
      </div>
    </div>
  );
}
