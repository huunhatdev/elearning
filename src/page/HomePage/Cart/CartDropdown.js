import React, { useState } from "react";
import { Dropdown, Typography } from "antd";
import { ShoppingCartOutlined } from "@ant-design/icons";
import "./Cart.css";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
export default function CartDropdown({ data }) {
  const { Text, Paragraph } = Typography;
  const SALE = useSelector((state) => state.cartSlice.saleOff);
  const PRICE = useSelector((state) => state.cartSlice.price);
  const [visible, setVisible] = useState(false);
  const handleVisibleChange = (flag) => {
    setVisible(flag);
  };
  let navigate = useNavigate();
  const cart = (
    <div className=" bg-white mt-8 shadow-md">
      {data?.map((e, i) => {
        return (
          <div className="p-5 border-b">
            <div className="flex justify-start items-start gap-2 w-64">
              <div>
                <img
                  src={e.hinhAnh}
                  alt=""
                  style={{ width: "100px", height: "100px" }}
                ></img>
              </div>
              <div className="w-1/2">
                <div>
                  <Paragraph
                    ellipsis={{ rows: 2 }}
                    className="font-bold text-sm leading-5"
                    style={{ marginBottom: "0px", lineHeight: "1" }}
                  >
                    {e.tenKhoaHoc}
                  </Paragraph>
                </div>
                <div>
                  <Text
                    ellipsis={{ rows: 1 }}
                    className="font-light text-xs"
                    style={{ color: "#6f7477" }}
                  >
                    {e.nguoiTao.hoTen}
                  </Text>
                </div>
                <div>
                  <span>
                    <span className="font-bold text-sm">
                      ${(PRICE * (1 - SALE)).toFixed(2)}
                    </span>{" "}
                    <span
                      className="line-through text-xs mx-1 font-light"
                      style={{ color: "#6a6f73" }}
                    >
                      ${PRICE}
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        );
      })}
      <div className="px-5 pb-4">
        <div>
          <span className="font-bold text-lg">
            Total: ${(PRICE * data.length * (1 - SALE)).toFixed(2)}
          </span>
          <span
            className="line-through text-sm mx-2 font-light"
            style={{ color: "#6a6f73" }}
          >
            ${(PRICE * data.length).toFixed(2)}
          </span>
        </div>
        {!window.location.pathname.includes("cart") ? (
          <div>
            <button
              className="w-64 bg-black text-white font-bold text-base py-4 px-4"
              onClick={(e) => {
                navigate("/cart");
                setVisible(false);
              }}
            >
              Go to cart
            </button>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
  const cartEmpty = (
    <div className="bg-white p-5 mt-8 shadow-md">
      <div>Cart is empty!</div>
    </div>
  );
  return (
    <div>
      <Dropdown
        overlay={data.length > 0 ? cart : cartEmpty}
        placement="bottomRight"
        visible={visible}
        onVisibleChange={handleVisibleChange}
      >
        <div className="relative">
          <ShoppingCartOutlined />
          {data.length ? (
            <span
              id="number-item-cart"
              className="w-5 h-5 flex justify-center items-center rounded-full font-bold text-white bg-violet-700 text-xs"
            >
              {data.length}
            </span>
          ) : (
            ""
          )}
        </div>
      </Dropdown>
    </div>
  );
}
