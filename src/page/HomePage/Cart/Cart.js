import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { localStorageService } from "../../../services/localStorageService";
import CartItem from "./CartItem";
import swal from "sweetalert";
import { courseService } from "../../../services/courseService";
import { confirmItem } from "../../../redux/Slices/cartSlice";
import { useNavigate } from "react-router";

export default function Cart() {
  const cart = useSelector((state) => state.cartSlice.cart);
  const SALE = useSelector((state) => state.cartSlice.saleOff);
  const PRICE = useSelector((state) => state.cartSlice.price);
  let [register, setRegister] = useState({
    flag: "",
    err: "",
    id: "",
  });
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let confirmCourse = (itemList) => {
    let userInfo = localStorageService.getUserInfo();
    if (!userInfo) {
      swal({
        title: "Bạn chưa đăng nhập?",
        text: "Bạn có muốn đăng nhập không?",
        icon: "error",
        buttons: ["Không", "Đăng nhập"],
      }).then((dangNhap) => {
        if (dangNhap) {
          setTimeout(() => {
            navigate("/login");
          }, 2000);
        } else {
          navigate("/");
        }
      });
    } else {
      if (cart.length === 0) {
        swal({
          title: "Đăng ký thất bại",
          text: "Mời bạn chọn khoá học",
          icon: "error",
          button: "Đồng ý",
        });
      } else {
        swal({
          title: "Xác nhận đăng ký",
          text: "Bạn xác nhận đăng ký khoá học?",
          icon: "warning",
          buttons: ["Không", "Xác nhận"],
        }).then((willConfirm) => {
          if (willConfirm) {
            itemList.forEach((item) => {
              let obj = {
                maKhoaHoc: item.maKhoaHoc,
                taiKhoan: localStorageService.getUserInfo()?.taiKhoan,
              };
              courseService
                .postDangKyKhoaHoc(obj)
                .then((res) => {
                  setRegister({ flag: true, err: "", id: "" });
                })
                .catch((err) => {
                  setRegister({
                    flag: false,
                    err: err.response.data,
                    id: obj.maKhoaHoc,
                  });
                });
            });
          }
        });
      }
    }
  };
  useEffect(() => {
    if (register.flag !== "") {
      if (register.flag && !register.err) {
        dispatch(confirmItem(cart));
        swal({
          title: "Đăng ký thành công",
          text: "Cảm ơn bạn đã đăng ký khóa học",
          icon: "success",
          button: "Đồng ý",
        });
      } else {
        swal({
          title: `Đăng ký ${register.id} thất bại`,
          text: register.err,
          icon: "error",
          button: "Đồng ý",
        });
      }
      setRegister({
        flag: "",
        err: "",
        id: "",
      });
    }
  }, [register.flag]);
  return (
    <div className="container mx-auto text-left" style={{ minHeight: "600px" }}>
      <h1 className=" text-4xl my-10 font-bold" id="cart-header">
        Shopping Cart
      </h1>

      <div className="grid grid-cols-6">
        <div className=" col-span-4">
          <h2 className=" text-lg font-light">{cart.length} Course in Cart</h2>
          {cart?.map((e, i) => {
            return <CartItem data={e} key={i} sale={SALE} price={PRICE} />;
          })}
        </div>
        <div className=" col-span-2 pl-5">
          <h2 className=" text-lg font-light">Total:</h2>
          <span className="font-bold text-4xl">
            ${(PRICE * cart.length * (1 - SALE)).toFixed(2)}
          </span>{" "}
          <span
            className="line-through text-base mx-1 font-light"
            style={{ color: "#6a6f73" }}
          >
            ${(PRICE * cart.length).toFixed(2)}
          </span>
          <p>{SALE * 100}% off</p>
          <div className="border-b pb-2">
            <button
              className="w-full font-bold text-white text-base py-4 bg-violet-600 hover:bg-violet-700"
              onClick={() => {
                confirmCourse(cart);
              }}
            >
              Checkout
            </button>
          </div>
          <div>
            <p className="font-bold text-base mb-0">Promotions</p>
            <div>
              <button className="p-2 text-lg font-medium">X</button>
              <span className=" font-light text-gray-500">
                <span className="font-bold">ST2MT8322</span> is applied
              </span>
            </div>
          </div>
          <div className="flex">
            <input
              placeholder="Enter Coupon"
              className="border-black border p-2 grow outline-none"
            ></input>
            <button className="font-bold text-white bg-violet-600 hover:bg-violet-700 py-2 px-4">
              Apply
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
