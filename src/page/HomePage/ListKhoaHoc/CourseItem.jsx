import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { Rate, Popover } from "antd";
import PopupCourse from "./PopupCourse";
import { courseService } from "../../../services/courseService";
export default function CourseItem({ item, index }) {
  const navigate = useNavigate();

  const [listHV, setListHV] = useState([]);
  useEffect(() => {
    document.getElementById("search-course").value = "";
    let fetchData = async () => {
      let result = await courseService.getThongTinHocVienKhoaHoc(
        item.maKhoaHoc
      );
      setListHV(result.data.lstHocVien);
    };
    fetchData();
  }, []);
  const content = <PopupCourse data={item} listHV={listHV} />;
  return (
    <div className="cursor-pointer">
      <Popover placement={index < 3 ? "right" : "left"} content={content}>
        <div
          onClick={() => {
            navigate(`/course/${item.maKhoaHoc}`);
          }}
        >
          <img src={item.hinhAnh} className="w-full h-32 relative" alt="" />
          <div className="text-left">
            <span className=" text-base font-bold mt-2">{item.tenKhoaHoc}</span>
            <div className=" leading-3 whitespace-normal">
              <span className=" text-xs text-gray-500 font-light">
                {item.nguoiTao.hoTen}
              </span>
              <div className="flex justify-start items-center h-5">
                <span
                  className="font-bold text-sm"
                  style={{ color: "#b4690f" }}
                >
                  4.5
                </span>
                <Rate
                  disabled
                  allowHalf
                  defaultValue={4.5}
                  style={{ fontSize: 12, color: "#e59819", margin: "0 4px" }}
                />
                <span className="text-xs text-gray-500 font-light">
                  ({item.luotXem})
                </span>
              </div>
              <div>
                <span>
                  <span className="font-bold text-base">$16.99</span>{" "}
                  <span
                    className="line-through text-sm mx-2 font-light"
                    style={{ color: "#6a6f73" }}
                  >
                    $84.99
                  </span>
                </span>
              </div>
            </div>
          </div>
          <div></div>
        </div>
      </Popover>
    </div>
  );
}
