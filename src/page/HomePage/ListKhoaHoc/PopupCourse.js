import React from "react";
import { CheckOutlined, HeartOutlined } from "@ant-design/icons";
import { Tag } from "antd";
import { useDispatch } from "react-redux";
import { addItem } from "../../../redux/Slices/cartSlice";
import swal from "sweetalert";
import { localStorageService } from "../../../services/localStorageService";
export default function PopupCourse({ data, listHV }) {
  let dispatch = useDispatch();
  return (
    <div
      className="flex flex-col justify-center items-start text-left font-light"
      style={{ width: "292px" }}
    >
      <span className=" text-lg font-bold my-2">{data.tenKhoaHoc}</span>
      <span className="text-xs text-green-700">
        <Tag color="lime">Best seller</Tag> Updated
        <span className="font-bold mx-1">{data.ngayTao}</span>
      </span>
      <span className=" my-2">
        {" "}
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Itaque eaque,
        exercitationem magni maxime vitae ipsa velit nostrum accusantium tenetur
        nemo.
      </span>
      <div className="flex justify-start items-start text-left">
        <div>
          <CheckOutlined />
        </div>
        <span className="ml-4 ">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit.
        </span>
      </div>
      <div className="flex justify-start items-start text-left">
        <div>
          <CheckOutlined />
        </div>
        <span className="ml-4">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit.
        </span>
      </div>
      <div className="flex justify-start items-start text-left">
        <div>
          <CheckOutlined />
        </div>
        <span className="ml-4">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit.
        </span>
      </div>
      <div className="flex justify-between items-center mt-2 w-full">
        <button
          className=" w-full text-white font-bold py-4 text-base mb-2 bg-purple-600 hover:bg-purple-700"
          onClick={() => {
            let index = listHV.findIndex((item) => {
              return (
                item.taiKhoan === localStorageService.getUserInfo()?.taiKhoan
              );
            });
            if (index !== -1) {
              swal({
                title: "Bạn đã đăng ký khoá học này",
                text: "Vui lòng kiểm tra danh sách khóa học đã đăng ký",
                icon: "error",
                button: "Đồng ý",
              });
            } else {
              dispatch(addItem(data));
            }
          }}
        >
          Add to cart
        </button>
        <div className="mx-2">
          <button className="border border-black hover:bg-slate-100 rounded-full w-12 h-12 flex justify-center items-center">
            <HeartOutlined style={{ fontSize: "16px" }} />
          </button>
        </div>
      </div>
    </div>
  );
}
