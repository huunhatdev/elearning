import React, { useEffect, useState } from "react";
import { courseService } from "../../../services/courseService";
import _ from "lodash";
import { Carousel } from "antd";
import CourseItem from "./CourseItem";
import { Link } from "react-scroll";
import {
  LeftOutlined,
  RightOutlined,
} from "@ant-design/icons";
export default function CourseList() {
  const [courseList, setCourseList] = useState([]);
  useEffect(() => {
    courseService
      .getDanhSachKhoaHoc()
      .then((res) => {
        let chunkList = _.chunk(res.data, 5);
        setCourseList(chunkList);
      })
      .catch((err) => {});
  }, []);

  useEffect(() => {
    let nextBtn = document.getElementById("nextBtn");
    let prevBtn = document.getElementById("prevBtn");
    if (nextBtn)
    {
        nextBtn.addEventListener("click",() => {
         let slide = prevBtn.getAttribute("currentslide");
        if (slide==="0")
        {
          prevBtn.setAttribute("style","width: 50px; height: 50px");
        }
       })
    }
    });
  return (
    <div className=" py-10">
      <div className=" px-28 container mx-auto">
        <div className=" text-left mb-8">
          <h1 className=" text-3xl font-bold">A broad selection of courses</h1>
          <h3 className=" text-xl">
            Choose from 185,000 online video courses with new additions
            published every month
          </h3>
        </div>
        <div className=" px-6 py-10" style={{ border: "1px solid gray" }}>
          <div className=" text-left w-2/3">
            <h1 className=" text-2xl font-bold">
              Expand your career opportunities with IT's courses
            </h1>
            <h3 className=" text-base font-light">
              Take one of Udemy’s range of Python courses and learn how to code
              using this incredibly useful language. Its simple syntax and
              readability makes Python perfect for Flask, Django, data science,
              and machine learning. You’ll learn how to build everything from
              games to sites to apps. Choose from a range of courses that will
              appeal to
            </h3>
            <Link
              to="course"
              spy={true}
              smooth={true}
              offset={-150}
              duration={500}
            >
              <div
                className=" px-2 py-2 font-bold w-36 text-center cursor-pointer text-black my-6"
                style={{ border: "1px solid black" }}
              >
                Explore Course
              </div>
            </Link>
          </div>
          <div name="course">
            <Carousel arrows={true} prevArrow={<LeftOutlined id="prevBtn"/>} nextArrow={<RightOutlined id="nextBtn"/>} >
              {courseList.map((course, index) => {
                return (
                  <div className="h-max w-full pt-5" key={index}>
                    <div className="grid grid-cols-5 gap-5">
                      {course.map((e,i) => {
                        return <CourseItem item={e} index={i}/>;
                      })}
                    </div>
                  </div>
                );
              })}
            </Carousel>
          </div>
        </div>
      </div>
    </div>
  );
}
