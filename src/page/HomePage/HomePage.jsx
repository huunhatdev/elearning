import { Carousel } from "antd";
import React from "react";
import CourseList from "./ListKhoaHoc/courseList";
import "../css/page.css";
export default function HomePage() {
  return (
    <div>
      <div className=" px-20 container mx-auto">
        <Carousel>
          <div className=" relative z-0">
            <img
              src="https://img-c.udemycdn.com/notices/web_banner/slide_1_image_udlite/7869fd17-9599-4a5e-9b03-4748a3dae016.jpg"
              alt=""
              className=" z-0"
            />
            <div className=" py-5 px-4 w-1/3 h-max bg-white z-10 absolute text-black top-16 left-20 text-left">
              <h1 className=" text-3xl font-bold ">
                Unlock the power of your people
              </h1>
              <span className=" text-lg">
                Udemy Business is trusted by 10.5K+ companies around the world.
                <span
                  className="underline cursor-pointer"
                  style={{ color: "#5624d0" }}
                >
                  Find out what we can do for yours.
                </span>
              </span>
            </div>
          </div>
          <div className=" relative z-0">
            <img
              src="https://img-c.udemycdn.com/notices/web_banner/slide_2_image_udlite/e6cc1a30-2dec-4dc5-b0f2-c5b656909d5b.jpg"
              alt=""
              className=" z-0"
            />
            <div className=" py-5 px-4 w-1/3 h-max bg-white z-10 absolute text-black top-16 left-20 text-left">
              <h1 className=" text-3xl font-extrabold ">
                Learning that gets you
              </h1>
              <span className=" text-lg font-medium">
                Skills for your present (and your future). Get started with us.
              </span>
            </div>
          </div>
        </Carousel>
      </div>
      <CourseList />
      <div className=" container mx-auto">
        <div className=" flex justify-center mb-16">
          <div className=" flex gap-24">
            <img
              src="https://s.udemycdn.com/home/non-student-cta/instructor-1x-v3.jpg"
              alt=""
            />
            <div className=" text-left mt-24" style={{ color: "#1c1d1f" }}>
              <h4 className=" text-4xl font-bold">Become an instructor</h4>
              <div
                className="  font-normal my-7"
                style={{ fontSize: "1.2rem", lineHeight: "0.3" }}
              >
                <p>Instructors from around the world teach</p>
                <p>millions of students on Udemy. We provide the</p>
                <p>tools and skills to teach what you love.</p>
              </div>
              <div className=" px-1 bg-black text-white font-extrabold w-48 h-12 justify-center flex items-center text-base cursor-pointer">
                Start teaching today
              </div>
            </div>
          </div>
        </div>
        <div
          className=" flex justify-center items-center py-5"
          style={{ backgroundColor: "#f7f9fa" }}
        >
          <div>
            <h1 className=" text-2xl font-bold">
              Trusted by companies of all sizes
            </h1>
            <div className="flex cursor-pointer">
              <img
                src="https://s.udemycdn.com/partner-logos/v4/nasdaq-dark.svg"
                alt=""
              />
              <img
                src="https://s.udemycdn.com/partner-logos/v4/volkswagen-dark.svg"
                alt=""
              />
              <img
                src="https://s.udemycdn.com/partner-logos/v4/box-dark.svg"
                alt=""
              />
              <img
                src="https://s.udemycdn.com/partner-logos/v4/netapp-dark.svg"
                alt=""
              />
              <img
                src="https://s.udemycdn.com/partner-logos/v4/eventbrite-dark.svg"
                alt=""
              />
            </div>
          </div>
        </div>
        <div className=" flex justify-center my-16">
          <div className=" flex gap-24">
            <div className=" text-left mt-24" style={{ color: "#1c1d1f" }}>
              <img
                src="https://www.udemy.com/staticx/udemy/images/v7/logo-ub.svg"
                alt=""
                className=" h-12"
              />
              <div
                className="font-normal my-7"
                style={{ fontSize: "1.2rem", lineHeight: "0.3" }}
              >
                <p>Get unlimited access to 16,000+ of Udemy’s</p>
                <p>top courses for your team. Learn and improve</p>
                <p>skills across business, tech, design, and more.</p>
              </div>
              <div className=" px-2 bg-black text-white font-extrabold w-48 h-12 justify-center flex items-center text-base cursor-pointer">
                Get Udemy Business
              </div>
            </div>
            <img
              src="https://s.udemycdn.com/home/non-student-cta/ub-1x-v3.jpg"
              alt=""
            />
          </div>
        </div>
        <div className=" flex justify-center mb-32">
          <div className=" flex gap-24">
            <img
              src="https://s.udemycdn.com/home/non-student-cta/transform-1x-v3.jpg"
              alt=""
            />
            <div
              className=" text-left mt-24"
              style={{ color: "#1c1d1f", lineHeight: "0.1" }}
            >
              <div className=" w-96">
                <h4 className=" text-3xl font-bold">
                  Transform your life through education
                </h4>
              </div>
              <div
                className="font-normal my-7"
                style={{ fontSize: "1.2rem", lineHeight: "0.3" }}
              >
                <p>Learners around the world are launching new</p>
                <p>careers, advancing in their fields, and enriching</p>
                <p>their lives.</p>
              </div>
              <div className="bg-black text-white font-extrabold w-32 h-12 justify-center flex items-center text-base cursor-pointer">
                Find out how
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
