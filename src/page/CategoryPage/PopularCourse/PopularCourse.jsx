import React from "react";
import { Carousel } from "antd";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router";

export default function PopularCourse({ categoryList }) {
  let navigate = useNavigate();
  return (
    <div className="mb-10">
      <Carousel
        arrows={true}
        prevArrow={<LeftOutlined id="prevBtn" />}
        nextArrow={<RightOutlined id="nextBtn" />}
      >
        <div className="h-max w-full pt-5">
          <div className="grid grid-cols-5 gap-2">
            {categoryList.map((courseName, index) => {
              return (
                <button
                  className="border py-5 px-6"
                  key={index}
                  onClick={() => {
                    navigate(`/courses/${courseName.maDanhMuc}`);
                  }}
                >
                  <span className=" text-base">{courseName.tenDanhMuc}</span>
                </button>
              );
            })}
          </div>
        </div>
      </Carousel>
    </div>
  );
}
