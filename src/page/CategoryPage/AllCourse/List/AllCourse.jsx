import React from "react";
import { List } from "antd";
import { useNavigate } from "react-router";
import { Rate, Typography } from "antd";
export default function AllCourse({ rawData }) {
  const { Paragraph, Text } = Typography;
  let navigate = useNavigate();
  return (
    <div>
      <List
        itemLayout="vertical"
        size="large"
        pagination={{
          pageSize: 5,
        }}
        dataSource={rawData}
        renderItem={(item) => (
          <List.Item key={item.key}>
            <div
              className=" h-max cursor-pointer flex justify-start items-start"
              onClick={() => {
                navigate(`/course/${item.maKhoaHoc}`);
              }}
            >
              <div className="w-1/4">
                <img
                  src={item.hinhAnh}
                  style={{ width: "300px", height: "180px" }}
                  alt=""
                />
              </div>
              <div className="text-left pl-8 leading-5 flex flex-col justify-between w-1/2">
                <div>
                  <span className=" text-3xl font-bold">{item.tenKhoaHoc}</span>
                  <Paragraph
                    ellipsis={{ rows: 2 }}
                    className="text-base font-light"
                    style={{ marginBottom: "0px" }}
                  >
                    {item.moTa}
                  </Paragraph>
                  <div className="whitespace-normal font-light">
                    <span className="text-xs text-gray-500">
                      {" "}
                      By {item.nguoiTao.hoTen}
                    </span>
                    <div className="flex justify-start items-center">
                      <span
                        className="font-bold text-sm"
                        style={{ color: "#b4690f" }}
                      >
                        4.5
                      </span>
                      <Rate
                        disabled
                        allowHalf
                        defaultValue={4.5}
                        style={{
                          fontSize: "14px",
                          color: "#e59819",
                          margin: "0 4px",
                          paddingBottom: "2px",
                        }}
                      />
                      <span className="text-xs text-gray-500">
                        ({item.luotXem})
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="w-1/4 justify-center items-center flex flex-col">
                <span className="font-bold text-lg">$16.99</span>{" "}
                <span
                  className="line-through text-sm mx-2 font-light"
                  style={{ color: "#6a6f73" }}
                >
                  $84.99
                </span>
              </div>
            </div>
          </List.Item>
        )}
      />
    </div>
  );
}
