import React, { useState } from "react";
import { Layout, Menu, Select } from "antd";
export default function FilterSideBar() {
  const { Sider } = Layout;
  const { Option } = Select;
  const [collapsed, setCollapsed] = useState(false);
  const handleChange = (value) => {};
  return (
    <div>
      <button
        onClick={() => {
          setCollapsed(!collapsed);
        }}
      >
        Filter
      </button>
      <Select
        defaultValue="lucy"
        style={{
          width: 120,
        }}
        onChange={handleChange}
      >
        <Option value="jack">Jack</Option>
        <Option value="lucy">Lucy</Option>
        <Option value="Yiminghe">yiminghe</Option>
      </Select>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={(broken) => {}}
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
        trigger={null}
      >
        <Menu theme="light" mode="inline">
          <Menu.Item key="1" className="items-center">
            <span>test</span>
          </Menu.Item>
        </Menu>
      </Sider>
    </div>
  );
}
