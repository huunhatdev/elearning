import React from "react";
import "./FeaturedCourse.css";
import { Carousel, Rate, Typography } from "antd";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router";
export default function FeaturedCourse({ rawData }) {
  let navigate = useNavigate();
  const { Paragraph, Text } = Typography;
  return (
    <div className="mb-10">
      <Carousel
        arrows={true}
        prevArrow={<LeftOutlined />}
        nextArrow={<RightOutlined />}
      >
        {rawData.map((course, index) => {
          return (
            <div className="border p-5" id="featured-course">
              <div
                className="cursor-pointer flex justify-start items-start h-full"
                onClick={() => {
                  navigate(`/detail/${course.maKhoaHoc}`);
                }}
              >
                <div>
                  <img
                    src={course.hinhAnh}
                    style={{ width: "480px", height: "270px" }}
                    alt=""
                  />
                </div>
                <div
                  className="w-1/2 h-full text-left pl-8 leading-5 flex flex-col justify-between"
                  style={{ height: "270px" }}
                >
                  <div>
                    <span className=" text-3xl font-bold">
                      {course.tenKhoaHoc}
                    </span>
                    <Paragraph
                      ellipsis={{ rows: 5 }}
                      className="text-base font-light"
                      style={{ marginBottom: "0px" }}
                    >
                      {course.moTa}
                    </Paragraph>
                    <div className="whitespace-normal font-light">
                      <span className="text-xs text-gray-500">
                        {" "}
                        By {course.nguoiTao.hoTen}
                      </span>
                      <div className="flex justify-start items-center">
                        <span
                          className="font-bold text-sm"
                          style={{ color: "#b4690f" }}
                        >
                          4.5
                        </span>
                        <Rate
                          disabled
                          allowHalf
                          defaultValue={4.5}
                          style={{
                            fontSize: "14px",
                            color: "#e59819",
                            margin: "0 4px",
                            paddingBottom: "2px",
                          }}
                        />
                        <span className="text-xs text-gray-500">
                          ({course.luotXem})
                        </span>
                      </div>
                    </div>
                  </div>
                  <div>
                    <span className="font-bold text-lg">$16.99</span>{" "}
                    <span
                      className="line-through text-sm mx-2 font-light"
                      style={{ color: "#6a6f73" }}
                    >
                      $84.99
                    </span>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </Carousel>
    </div>
  );
}
