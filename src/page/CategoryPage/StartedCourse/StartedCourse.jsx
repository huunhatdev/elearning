import React from "react";
import { Tabs, Carousel } from "antd";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import CourseItem from "../../HomePage/ListKhoaHoc/CourseItem";
export default function StartedCourse({ courseList }) {
  const { TabPane } = Tabs;
  let renderCourse = () => {
    return (
      <Carousel
        arrows={true}
        prevArrow={<LeftOutlined id="prevBtn" />}
        nextArrow={<RightOutlined id="nextBtn" />}
      >
        {courseList.map((course, index) => {
          return (
            <div className="h-max w-full pt-5" key={index}>
              <div className="grid grid-cols-5 gap-5">
                {course.map((item, index) => {
                  return <CourseItem item={item} key={index} />;
                })}
              </div>
            </div>
          );
        })}
      </Carousel>
    );
  };
  return (
    <div className=" mb-10">
      <Tabs defaultActiveKey="1">
        <TabPane
          tab={<span className="font-bold text-base">Most popular</span>}
          key="1"
        >
          {renderCourse()}
        </TabPane>
        <TabPane
          tab={<span className="font-bold  text-base">New</span>}
          key="2"
        >
          {renderCourse()}
        </TabPane>
        <TabPane
          tab={<span className="font-bold text-base">Trending</span>}
          key="3"
        >
          {renderCourse()}
        </TabPane>
      </Tabs>
    </div>
  );
}
