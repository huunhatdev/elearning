import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { courseService } from "../../services/courseService";
import { MoreOutlined } from "@ant-design/icons";
import _, { set } from "lodash";
import { Menu, Dropdown, Space } from "antd";
import StartedCourse from "./StartedCourse/StartedCourse";
import FeaturedCourse from "./FeaturedCourse/FeaturedCourse";
import PopularCourse from "./PopularCourse/PopularCourse";
import AllCourse from "./AllCourse/List/AllCourse";
import "./CategoryPage.css";
export default function CategoryPage() {
  let maDanhMuc = useParams();
  let [rawData, setRawData] = useState([]);
  let [courseList, setCourseList] = useState([]);
  let [courseCategory, setCourseCategory] = useState("");
  let [categoryList, setCategoryList] = useState([]);

  useEffect(() => {
    let fetchCategory = async () => {
      let result = await courseService.getDanhMucKhoaHoc();
      setCategoryList(result.data);
    };
    fetchCategory();
  }, []);
  useEffect(() => {
    let fetchData = async () => {
      let result = await courseService.getDSKhoaHocTheoDanhMuc(maDanhMuc.id);
      let chunkList = _.chunk(result.data, 5);
      setRawData(result.data);
      setCourseList(chunkList);
      setCourseCategory(chunkList[0][0].danhMucKhoaHoc.tenDanhMucKhoaHoc);
    };
    fetchData();
  }, [maDanhMuc]);
  const menu = (
    <Menu
      items={[
        {
          label: <a href="https://www.antgroup.com">1st menu item</a>,
          key: "0",
        },
        {
          label: <a href="https://www.aliyun.com">2nd menu item</a>,
          key: "1",
        },
        {
          type: "divider",
        },
        {
          label: "3rd menu item",
          key: "3",
        },
      ]}
    />
  );
  return (
    <div>
      <div className=" bg-white z-10 shadow-md border-t text-left flex justify-between items-center px-10">
        <nav>
          <ul className="mb-0">
            <li className="flex justify-center items-center">
              <a href="" className="font-bold text-black hover:text-violet-700">
                {courseCategory}
              </a>
              <img
                width={18}
                height={56}
                src="https://s.udemycdn.com/browse_components/link-bar/large-next.svg"
                alt=""
              ></img>
            </li>
          </ul>
        </nav>
        <div>
          <Dropdown overlay={menu} trigger={["click"]}>
            <a onClick={(e) => e.preventDefault()}>
              <Space>
                <MoreOutlined style={{ fontSize: "28px", fontWeight: "700" }} />
              </Space>
            </a>
          </Dropdown>
        </div>
      </div>
      <div className="container mx-auto w-full my-10 font-bold text-left">
        <h1 className=" text-3xl my-10" id="category-header">
          {courseCategory} Course
        </h1>
        <h2 className=" text-2xl font-bold">Course to get you started</h2>
        <StartedCourse courseList={courseList} />
        <h2 className="  text-2xl font-bold">Featured courses</h2>
        <FeaturedCourse rawData={rawData} />
        <h2 className="  text-2xl font-bold">Popular topics</h2>
        <PopularCourse categoryList={categoryList} />
        <h2 className=" text-2xl font-bold">All courses</h2>
        <div>
          <AllCourse rawData={rawData} />
        </div>
      </div>
    </div>
  );
}
