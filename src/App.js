import "./App.css";
import "antd/dist/antd.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { userRouter } from "./routes/userRouter";
import LayoutTheme from "./HOC/LayoutTheme";
import SpinnerComponent from "./components/Spinner/SpinnerTheme";

function App() {
  return (
    <div className="App">
      <SpinnerComponent />
      <BrowserRouter>
        <Routes>
          {userRouter.map((e, i) => {
            if (e.isUseLayout) {
              return (
                <Route
                  key={i}
                  path={e.path}
                  element={<LayoutTheme component={e.component} />}
                />
              );
            }
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
