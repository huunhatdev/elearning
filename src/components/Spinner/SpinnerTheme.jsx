import React from "react";
import { RingLoader } from "react-spinners";
import { useSelector } from "react-redux";
export default function SpinnerComponent() {
  const isLoading = useSelector((state) => {
    return state.userSlice.isLoading;
  });
  return isLoading ? (
    <div className=" fixed h-screen w-screen bg-black  z-50 flex items-center justify-center">
      <div>
        <RingLoader size={200} color={"#54ab34"} />
        <h1 className=" text-white text-2xl font-medium text-center mt-5">
          Loading ...
        </h1>
      </div>
    </div>
  ) : (
    <></>
  );
}
