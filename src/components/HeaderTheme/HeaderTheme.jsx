import React, { useEffect, useState } from "react";
import "./header.css";
import {
  GlobalOutlined,
  RightOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { DownOutlined } from "@ant-design/icons";
import { Dropdown, Menu, Space, Input } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { courseService } from "../../services/courseService";
import CartDropdown from "../../page/HomePage/Cart/CartDropdown";
import { useDispatch, useSelector } from "react-redux";
import { localStorageService } from "../../services/localStorageService";
import SearchCourse from "./SearchCourse";
const { Search } = Input;
const menu = (
  <Menu
    items={[
      {
        key: "1",
        label: (
          <div className="flex text-xs justify-between ">
            <p>Development</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "2",
        label: (
          <div className="flex text-xs justify-between">
            <p>Business</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "3",
        label: (
          <div className="flex text-xs justify-between">
            <p>Finance & Accounting</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "4",
        label: (
          <div className="flex text-xs justify-between">
            <p>IT & Software</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "5",
        label: (
          <div className="flex text-xs justify-between">
            <p>Office Productivity</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "6",
        label: (
          <div className="flex text-xs justify-between">
            <p>Personal Development</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "7",
        label: (
          <div className="flex text-xs justify-between">
            <p>Design</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "8",
        label: (
          <div className="flex text-xs justify-between">
            <p>Marketing</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "9",
        label: (
          <div className="flex text-xs justify-between">
            <p>Lifestyle</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "10",
        label: (
          <div className="flex text-xs justify-between">
            <p>Photography & Video</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "11",
        label: (
          <div className="flex text-xs justify-between">
            <p>Health & Fitness</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "12",
        label: (
          <div className="flex text-xs justify-between">
            <p>Music</p>
            <RightOutlined />
          </div>
        ),
      },
      {
        key: "13",
        label: (
          <div className="flex text-xs justify-between">
            <p>Teaching & Academics</p>
            <RightOutlined />
          </div>
        ),
      },
    ]}
  />
);
export default function HeaderTheme() {
  let dispatch = useDispatch();
  const [danhSachKhoaHoc, setDanhSachKhoaHoc] = useState([]);
  const [dataSearch, setDataSearch] = useState(null);
  useEffect(() => {
    courseService
      .getDanhSachKhoaHoc()
      .then((res) => {
        setDanhSachKhoaHoc(res.data);
      })
      .catch((err) => {});
  }, []);
  let [course, setCourseList] = useState([]);
  let courseItem = useSelector((state) => state.cartSlice.cart);
  let navigate = useNavigate();
  useEffect(() => {
    let fetchCourse = async () => {
      let courselist = await courseService.getDanhMucKhoaHoc();
      let menu = [];
      courselist.data.map((item, index) => {
        let obj = {
          key: index + 1,
          label: (
            <div
              className="flex text-xs justify-between items-center py-2"
              onClick={() => {
                navigate(`/courses/${item.maDanhMuc}`);
              }}
            >
              <p className="mb-0">{item.tenDanhMuc}</p>
             <RightOutlined className="mx-2"/>
          </div>
        )
      }
      menu.push(obj);
    })
   setCourseList(menu);
  }
  fetchCourse();
 },[]);
 const adminMenu = (
  <Menu
    items={[
      {
        key: "1",
        label: (
          <div className="hover:text-blue-500 duration-150" onClick={()=> window.open("https://udemy-admin.vercel.app/", "_blank")}>
            <p className="mb-0 text-sm flex justify-center">Admin page</p>
          </div>
        ),
      }]}
      />
    );
 const categories = (<Menu
  items={course
  }/>);

  let userInfo = useSelector((state) => {
    return state.userSlice.userInfo;
  });
  let handleLogout = () => {
    localStorageService.removeUserInfo();
    window.location.href = "/login";
  };
  const onChange = (e) => {
    let text = e.target.value;
    if (!text) {
      setDataSearch(null);
    } else {
      let arr = danhSachKhoaHoc.filter((e) => {
        return e.tenKhoaHoc.toUpperCase().includes(text.toUpperCase());
      });
      setDataSearch(arr);
    }
  };
  if (userInfo == null) {
    return (
      <header className="w-full py-4">
        <div className=" flex items-center w-full px-4">
          <NavLink to={"/"}>
            <img
              className="w-56 h-18 ml-2 "
              src="https://www.udemy.com/staticx/udemy/images/v7/logo-udemy.svg"
              alt=""
            />
          </NavLink>
          <div className="px-5">
            <Dropdown overlay={categories}>
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  Categories
                  <DownOutlined />
                </Space>
              </a>
            </Dropdown>
          </div>
          <div
            className=" flex rounded-3xl mr-5 h-12 relative"
            style={{ border: "1px solid black", width: "120%" }}
          >
            <div className=" pt-2 px-3">
              <SearchOutlined />
            </div>
            <input
              style={{ width: "90%", position: "relative" }}
              className=" outline-none"
              id="search-course"
              onChange={(e) => {
                onChange(e);
              }}
              type="text"
              placeholder="Search for anything"
            />
            {dataSearch && <SearchCourse dataSearch={dataSearch} />}
          </div>
          <div className=" flex items-center justify-evenly w-full">
            <Dropdown overlay={menu}>
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  Udemy Business
                  <DownOutlined />
                </Space>
              </a>
            </Dropdown>
            <Dropdown overlay={menu}>
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  Teach on Udemy
                  <DownOutlined />
                </Space>
              </a>
            </Dropdown>
            <NavLink to={"/cart"}>
              <CartDropdown data={courseItem} />
            </NavLink>
            <div className=" flex gap-1">
              <NavLink to={"/login"}>
                <div
                  className=" px-4 py-2 font-bold cursor-pointer hover:bg-gray-100 duration-150 text-black"
                  style={{ border: "1px solid black" }}
                >
                  Log in
                </div>
              </NavLink>
              <div className=" flex gap-1">
                  <NavLink to={"/login"}>
                    <div
                      className=" px-4 py-2 font-bold cursor-pointer hover:bg-gray-100 duration-150 text-black"
                      style={{ border: "1px solid black" }}
                    >
                      Log in
                    </div>
                  </NavLink>
                  <NavLink to={"/register"}>
                  <div
                    className=" px-4 py-2 font-bold bg-black text-white cursor-pointer"
                    style={{ border: "1px solid black" }}
                  >
                    Register
                  </div>
                  </NavLink>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  } else {
    return (
      <header className="w-full py-4">
        <div className=" flex items-center w-full px-4">
          <NavLink to={"/"}>
            <img
              className="w-56 h-18 ml-2 "
              src="https://www.udemy.com/staticx/udemy/images/v7/logo-udemy.svg"
              alt=""
            />
          </NavLink>
          <div className="px-5">
            <Dropdown overlay={categories}>
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  Categories
                  <DownOutlined />
                </Space>
              </a>
            </Dropdown>
          </div>
          <div
            className=" flex rounded-3xl mr-5 h-12 relative"
            style={{ border: "1px solid black", width: "120%" }}
          >
            <div className=" pt-2 px-3">
              <SearchOutlined />
            </div>
            <input
              style={{ width: "90%", position: "relative" }}
              className=" outline-none"
              id="search-course"
              onChange={(e) => {
                onChange(e);
              }}
              type="text"
              placeholder="Search for anything"
            />
            {dataSearch && <SearchCourse dataSearch={dataSearch} />}
          </div>
          <div className=" flex items-center justify-evenly w-full">
            <Dropdown overlay={menu}>
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  Udemy Business
                  <DownOutlined />
                </Space>
              </a>
            </Dropdown>
            <Dropdown overlay={menu}>
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  Teach on Udemy
                  <DownOutlined />
                </Space>
              </a>
            </Dropdown>
            <NavLink to={"/cart"}>
              <CartDropdown data={courseItem} />
            </NavLink>
            <div className=" flex gap-1">
              <NavLink to="">
                <div
                  className=" px-4 py-2 font-bold cursor-pointer hover:bg-gray-100 duration-150 text-black"
                  style={{ border: "1px solid black" }}
                >
                  {userInfo.hoTen}
                </div>
              </NavLink>
              <div className=" flex gap-1">
                {localStorageService.getUserInfo()?.maLoaiNguoiDung==="GV"? (<Dropdown overlay={adminMenu}>
                      <div
                      className=" px-4 py-2 font-bold cursor-pointer hover:bg-gray-100 duration-150 text-black"
                      style={{ border: "1px solid black" }}
                     >
                      {userInfo.hoTen}
                      </div>
                  </Dropdown>):(
                    <NavLink to="">
                    <div
                      className=" px-4 py-2 font-bold cursor-pointer hover:bg-gray-100 duration-150 text-black"
                      style={{ border: "1px solid black" }}
                    >
                      {userInfo.hoTen}
                    </div>
                  </NavLink>
                  )}
                  <NavLink to={"/login"}>
                  <div
                    onClick={() => {
                      handleLogout();
                    }}
                    className=" px-4 py-2 font-bold bg-black text-white cursor-pointer"
                    style={{ border: "1px solid black" }}
                  >
                    Log Out
                  </div>
                  </NavLink>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}
