import React from "react";
import { useNavigate } from "react-router";

export default function SearchCourse({ dataSearch }) {
  const navigate = useNavigate();
  return (
    <div
      id="searchList"
      className=" z-50"
      style={{
        position: "absolute",
        bottom: "-300px",
        overflowY: "scroll",
        left: "6%",
        width: "93%",
        height: "300px",
        backgroundColor: "white",
        border: "1px solid black",
        borderRadius: "0 0 10px 10px",
      }}
    >
      {dataSearch.map((e) => {
        return (
          <div
            onClick={() => {
              navigate(`/course/${e.maKhoaHoc}`);
            }}
            className=" flex p-5 cursor-pointer hover:underline"
          >
            <img src={e.hinhAnh} className="w-20 h-16 mb-2" alt="" />
            <div className=" text-left ml-5">
              <h5>Tên Khóa Học : {e.tenKhoaHoc}</h5>
              <h5>Danh Mục Khóa Học : {e.danhMucKhoaHoc.tenDanhMucKhoaHoc}</h5>
            </div>
          </div>
        );
      })}
    </div>
  );
}
