import React from "react";

export default function SemiFooter() {
  return (
    <div
      className="py-7 flex px-12 "
      style={{ backgroundColor: "#1c1d1f", borderBottom: "1px solid gray" }}
    >
      <h4 className=" text-lg font-bold text-white mt-2">
        Top companies choose{" "}
        <span style={{ color: "#cec0fc" }}>Udemy Business</span> to build
        in-demand career skills.
      </h4>
      <div className=" flex ml-auto">
        <img
          alt="Nasdaq"
          height="{44}"
          width="{115}"
          src="https://s.udemycdn.com/partner-logos/v4/nasdaq-light.svg"
        />
        <img
          alt="Volkswagen"
          height="{44}"
          width="{44}"
          src="https://s.udemycdn.com/partner-logos/v4/volkswagen-light.svg"
        />
        <img
          alt="Box"
          height="{44}"
          width="{67}"
          src="https://s.udemycdn.com/partner-logos/v4/box-light.svg"
        />
        <img
          alt="NetApp"
          height="{44}"
          width="{115}"
          src="https://s.udemycdn.com/partner-logos/v4/netapp-light.svg"
        />
        <img
          alt="Eventbrite"
          height="{44}"
          width="{115}"
          src="https://s.udemycdn.com/partner-logos/v4/eventbrite-light.svg"
        />
      </div>
    </div>
  );
}
