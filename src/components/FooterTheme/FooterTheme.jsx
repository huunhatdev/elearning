import { GlobalOutlined } from "@ant-design/icons";
import React from "react";
import "./footer.css";
export default function FooterTheme() {
  return (
    <footer
      className=" w-full h-80 px-12 py-7"
      style={{ backgroundColor: "#1c1d1f" }}
    >
      <div className=" flex justify-between">
        <div className=" flex justify-between" style={{ width: "600px" }}>
          <div className=" text-white text-left leading-4">
            <p className=" hover:underline">Udemy Business</p>
            <p className=" hover:underline">Teach on Udemy</p>
            <p className=" hover:underline">Get the app</p>
            <p className=" hover:underline">About us</p>
            <p className=" hover:underline"> Contact us</p>
          </div>
          <div className=" text-white text-left leading-4">
            <p className=" hover:underline">Careers</p>
            <p className=" hover:underline">Blog</p>
            <p className=" hover:underline">Help and Support</p>
            <p className=" hover:underline">Affiliate</p>
            <p className=" hover:underline">Investors</p>
          </div>
          <div className=" text-white text-left leading-4">
            <p className=" hover:underline">Terms</p>
            <p className=" hover:underline">Privacy policy</p>
            <p className=" hover:underline">Cookie settings</p>
            <p className=" hover:underline">Sitemap</p>
            <p className=" hover:underline">Accessibility</p>
          </div>
        </div>
        <div
          className="font-medium text-white flex items-center gap-3 px-4 py-2 h-10 w-32 cursor-pointer hover:bg-gray-400"
          style={{ border: "1px solid white" }}
        >
          <GlobalOutlined />
          <span className="">English</span>
        </div>
      </div>
      <div className=" flex mt-16 justify-between">
        <img
          className="h-9"
          src="https://www.udemy.com/staticx/udemy/images/v7/logo-udemy-inverted.svg"
          alt=""
        />
        <p className=" text-white">© 2022 Udemy, Inc.</p>
      </div>
    </footer>
  );
}
