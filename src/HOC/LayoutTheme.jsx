import React from "react";
import FooterTheme from "../components/FooterTheme/FooterTheme";
import HeaderTheme from "../components/HeaderTheme/HeaderTheme";
import SemiFooter from "../components/SemiFooter/SemiFooter";

export default function LayoutTheme({ component }) {
  return (
    <div>
      <HeaderTheme />
      <>{component}</>
      <SemiFooter />
      <FooterTheme />
    </div>
  );
}
