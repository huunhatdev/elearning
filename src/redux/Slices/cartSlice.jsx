import { createSlice } from "@reduxjs/toolkit";
import { message } from "antd";

const initialState = {
  cart: [],
  saleOff: 0.8,
  price: 84.99,
};
const cartSlice = createSlice({
  name: "cart_slice",
  initialState,
  reducers: {
    addItem: (state, { payload }) => {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return payload.maKhoaHoc === item.maKhoaHoc;
      });
      if (index === -1) {
        cloneCart.push(payload);
        message.success("Đã thêm khóa học vào giỏ hàng");
      }
      state.cart = cloneCart;
    },
    removeItem: (state, { payload }) => {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return payload.maKhoaHoc === item.maKhoaHoc;
      });
      cloneCart.splice(index, 1);
      state.cart = cloneCart;
    },
    confirmItem: (state, { payload }) => {
      let cloneCart = [...state.cart];
      payload.forEach((course) => {
        let index = cloneCart.findIndex((item) => {
          return item.maKhoaHoc === course.maKhoaHoc;
        });
        cloneCart.splice(index, 1);
      });
      state.cart = cloneCart;
    },
  },
});
export const { addItem, removeItem, confirmItem } = cartSlice.actions;
export default cartSlice.reducer;
