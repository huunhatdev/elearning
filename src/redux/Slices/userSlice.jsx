import { createSlice } from "@reduxjs/toolkit";
import { localStorageService } from "../../services/localStorageService";

const initialState = {
  userInfo: localStorageService.getUserInfo(),
  user: {},
  isLoading: false,
  cart: [],
  searchCourse: null,
};
const userSlice = createSlice({
  name: "user_slice",
  initialState,
  reducers: {
    setUserInfor: (state, { payload }) => {
      state.userInfo = payload;
    },
    handleLoading: (state, { payload }) => {
      state.isLoading = payload;
    },
    handleAddToCart: (state, { payload }) => {
      let index = state.cart.findIndex((item) => {
        return item.maKhoaHoc === payload.maKhoaHoc;
      });
      if (index === -1) {
        state.cart.push({ ...payload, price: 12.99 });
      }
    },
    handleDeleteCourse: (state, { payload }) => {
      let index = state.cart.findIndex((item) => {
        return item.maKhoaHoc === payload.maKhoaHoc;
      });
      state.cart.splice(index, 1);
    },
  },
});
export const {
  demo,
  setUserInfor,
  handleLoading,
  handleAddToCart,
  handleDeleteCourse,
} = userSlice.actions;
export default userSlice.reducer;
