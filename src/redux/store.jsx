import { configureStore } from "@reduxjs/toolkit";
import cartSlice from "./Slices/cartSlice";
import userSlice from "./Slices/userSlice";

export const store = configureStore({
  reducer: {
    userSlice,
    cartSlice,
  },
});
